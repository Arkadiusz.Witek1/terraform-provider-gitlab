variable "CI_PROJECT_ID" {
  type        = number
  description = "The CI_PROJECT_ID from the current GitLab pipeline"
  default = "XXXXXXXXXXXX"
}