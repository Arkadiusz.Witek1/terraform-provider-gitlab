terraform {
  required_providers {
    gitlab = {
      source  = "registry.terraform.io/gitlabhq/gitlab"
      version = "16.0.3"
    }
  }
}