resource "gitlab_project_access_token" "token" {
  project    = var.CI_PROJECT_ID
  name       = "🤖 example-bot"
  expires_at = "2023-06-20"

  access_level = "maintainer"
  scopes       = ["read_api"]
  
  lifecycle {
    replace_triggered_by = [terraform_data.force_recreate]
  }
}