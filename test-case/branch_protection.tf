resource "gitlab_branch_protection" "this" {
  project = var.CI_PROJECT_ID
  branch  = "another-branch"

  push_access_level  = "no one"

  allowed_to_push {
    user_id = gitlab_project_access_token.token.user_id
  }
}